package com.ty.first_project.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

@Document(value = "journal")
@Getter
@Setter
public class Journal {
    private String id;
    private String title;
    private String description;
}
