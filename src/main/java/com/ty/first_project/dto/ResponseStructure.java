package com.ty.first_project.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class ResponseStructure<T> {
    private String message;
    private int statusCode;
    private T data;
}
