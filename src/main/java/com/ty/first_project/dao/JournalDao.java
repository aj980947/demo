package com.ty.first_project.dao;

import com.ty.first_project.entity.Journal;
import com.ty.first_project.repository.JournalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class JournalDao {
    @Autowired
    private JournalRepository repo;

    public Journal saveJournal(Journal journal) {
        return repo.save(journal);
    }
}
