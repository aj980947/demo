package com.ty.first_project.repository;

import com.ty.first_project.entity.Journal;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface JournalRepository extends MongoRepository<Journal,String> {
}
