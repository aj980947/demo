package com.ty.first_project.service;

import com.ty.first_project.dao.JournalDao;
import com.ty.first_project.dto.ResponseStructure;
import com.ty.first_project.entity.Journal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class JournalService {
    @Autowired
    private JournalDao dao;
    @Autowired
    private ResponseStructure structure;

    public ResponseEntity<?> saveJournal(Journal journal) {
       Journal dbJournal= dao.saveJournal(journal);
        structure.setMessage("saved");
        structure.setStatusCode(HttpStatus.CREATED.value());
        structure.setData(dbJournal);
        return new ResponseEntity<ResponseStructure<Journal>>(structure,HttpStatus.CREATED);
    }
}
